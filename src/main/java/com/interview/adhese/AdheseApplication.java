package com.interview.adhese;

public class AdheseApplication {

	public int calculateMinimalCostOfTheProjectFor(int[] minMonthlyPeopleAmount, int salary, int hire, int fire) {
		int totalCost = 0;
		int numberOfPpl = 0;

		for (int i = 0; i < minMonthlyPeopleAmount.length; i++) {
			totalCost += minMonthlyPeopleAmount[i] * salary;
			int difference = minMonthlyPeopleAmount[i] - numberOfPpl;
			numberOfPpl = minMonthlyPeopleAmount[i];
			if (difference > 0) {
				totalCost += difference * hire;
				numberOfPpl += difference;
			} else if (difference < 0) {
				int absoluteDifference = Math.abs(difference);

				if (absoluteDifference * fire > absoluteDifference * salary) {
					totalCost += absoluteDifference * salary;
					numberOfPpl+=absoluteDifference;
				} else {
					totalCost += absoluteDifference * fire;
				}
			}
		}

		return totalCost;
	}

}
