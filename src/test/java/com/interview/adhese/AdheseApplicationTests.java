package com.interview.adhese;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

public class AdheseApplicationTests {

	private AdheseApplication adheseApplication = new AdheseApplication();

	@ParameterizedTest
	@MethodSource("inputData")
	void sampleTest(int[] peopleCountForEachMonth, int salary, int hiringCost, int firingCost, int result) {
		int totalMinimalCost = adheseApplication.calculateMinimalCostOfTheProjectFor(peopleCountForEachMonth, salary, hiringCost, firingCost);

		assertThat(totalMinimalCost).isEqualTo(result);
	}

	@ParameterizedTest
	@MethodSource("inputData1")
	void nextInput(int[] peopleCountForEachMonth, int salary, int hiringCost, int firingCost) {
		System.out.println(adheseApplication.calculateMinimalCostOfTheProjectFor(peopleCountForEachMonth, salary, hiringCost, firingCost));
	}

	static Stream<Arguments> inputData() {
		return Stream.of(
				Arguments.of(new int[]{10, 9, 11}, 5, 4, 6, 199)
		);
	}

	static Stream<Arguments> inputData1() {
		return Stream.of(
				Arguments.of(new int[]{10, 9, 11}, 5, 4, 6),
				Arguments.of(new int[]{10, 9, 8, 11}, 5, 4, 6),
				Arguments.of(new int[]{10, 7, 8, 9, 5, 5, 9}, 5, 4, 7),
				Arguments.of(new int[]{10, 7, 8, 9, 5, 5, 9}, 5, 4, 4),
				Arguments.of(new int[]{1, 3, 1, 1, 3, 1}, 5, 4, 6),
				Arguments.of(new int[]{10, 9, 8, 7, 6, 5, 4, 3, 2, 1}, 4, 8, 8)
		);
	}
}
